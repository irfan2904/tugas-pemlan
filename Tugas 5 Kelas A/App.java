
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import com.config.cConfig;
import com.view.cView;

public class App extends JFrame {
    private JTextField textField1, textField2;
    private JButton displayButton, saveButton;
    private JTextArea textArea1, textArea2;

    public App() {
        setTitle("Database Mahasiswa");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // membuat label dan text field untuk NIM
        JLabel nimLabel = new JLabel("NIM");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(10, 10, 10, 10); // menambahkan jarak antar komponen
        add(nimLabel, gbc);

        // membuat text field untuk NIM
        textField1 = new JTextField(20);
        gbc.gridx = 1;
        gbc.gridy = 0;
        add(textField1, gbc);

        // membuat label dan text field untuk Nama
        JLabel namaLabel = new JLabel("Nama");
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        add(namaLabel, gbc);

        // membuat text field untuk Nama
        textField2 = new JTextField(20);
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(textField2, gbc);

        // membuat panel untuk tombol Display Data dan Save Data
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        displayButton = new JButton("Display Data");
        saveButton = new JButton("Save Data");
        buttonPanel.add(displayButton);
        buttonPanel.add(saveButton);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        add(buttonPanel, gbc);

        // membuat text area
        textArea1 = new JTextArea(10, 30);
        textArea2 = new JTextArea(2, 30);
        textArea1.setEditable(false);
        textArea2.setEditable(false);

        // membuat fitur scroll pada text area untuk display data dan text area untuk notif save data
        JScrollPane scrollPane1 = new JScrollPane(textArea1);
        JScrollPane scrollPane2 = new JScrollPane(textArea2);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        add(scrollPane1, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 2;
        add(scrollPane2, gbc);

        // menengahkan GUI
        pack();
        setLocationRelativeTo(null);
        
        // menambahkan action listener pada tombol Display Data dan Save Data
        displayButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                displayData();
            }
        });
        
        // menambahkan action listener pada tombol Save Data
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveData(textField1.getText() + " " + textField2.getText());
            }
        }); 
    }

    // menampilkan semua data pada database
    private void displayData() {
        textArea1.append(cView.getAllData());
    }

    // menampilkan notifikasi data berhasil disimpan
    private void saveData(String data) {
        textArea2.append(cConfig.saveData(data));
    }

    public static void main(String[] args) {

        // membuat GUI
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new App().setVisible(true);
            }
        });
    }
}