package com.config;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

public class cConfig {

    // mendefinisikan koneksi database
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost:3306/pemlan";

    private static final String USER = "root";
    private static final String PASS = "";

    // instansiasi object dari class yang sudah diimport
    private static Connection connect;
    private static Statement statement;
    private static ResultSet resultData;

    // method static connection
    private static void getConnection() { 
        try {
            // registrasi driver yang akan dipake
            Class.forName(JDBC_DRIVER);

            // buat koneksi ke database
            connect = DriverManager.getConnection(DB_URL, USER, PASS);

            //System.out.println("Koneksi berhasil");

        } catch (Exception e) {
            // kalo ada error saat koneksi
            e.printStackTrace();
        }
    }

    public static String getAllData() {
        cConfig.getConnection();

        // isi nilai default 
        String data = "maaf data tidak ada";

        try {

            // buat objek statement yang ambil dari koneksi
            statement = connect.createStatement();

            // query select all data from database
            String query = "select NIM, NAMA from pemlan.mahasiswa";

            // eksekusi query
            resultData = statement.executeQuery(query);

            // set variabel data jadi null
            data = "";

            // tampilkan data yang diambil
            while(resultData.next()) {
                data += "\nNIM\t: " + resultData.getString("NIM") + "\n" +
                        "Nama\t: " + resultData.getString("NAMA") + "\n";
            }

            // close statement dan connection
            statement.close();
            connect.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //System.out.println(data);
        return data;
    }

    public static String saveData(String data) {
        cConfig.getConnection();
    
        // isi nilai default
        String result = "Data gagal disimpan";
    
        try {
            // buat objek statement yang ambil dari koneksi
            statement = connect.createStatement();
    
            // memisahkan NIM dan nama berdasarkan spasi
            String[] parts = data.split(" ", 2);
            String nim = parts[0];
            String nama = parts[1];

            // query insert data ke database
            String query = "INSERT INTO pemlan.mahasiswa (NIM, NAMA) VALUES (' " + nim + " ', ' " + nama + " ')";

            // eksekusi query
            int rowsInserted = statement.executeUpdate(query);
    
            // menampilkan notifikasi data berhasil disimpan
            if (rowsInserted > 0) {
                result = "Data berhasil disimpan!";
            }
    
            // close statement dan connection
            statement.close();
            connect.close();
    
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        return result;
    }
}
