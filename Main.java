package StudiKasus2.src.p;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Mahasiswa> mahasiswa = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        boolean tambah = true;
        while (tambah) {

            System.out.print("Masukkan NIM : "); 
            String nim = in.nextLine();

            System.out.print("Masukkan Nama : ");
            String nama = in.nextLine();

            System.out.print("Masukkan Alamat : ");
            String alamat = in.nextLine();
            System.out.println("");

            Mahasiswa maha = new Mahasiswa(nim, nama, alamat);
            mahasiswa.add(maha);

            System.out.print("tambah lagi? (tekan 't' jika tidak mau tambah) ");
            String lanjut = in.nextLine();
            System.out.println("");

            if (lanjut.equals("t")) {
                tambah = false;
            }
        }

        in.close();

        System.out.println("==================================");
        for (Mahasiswa maha : mahasiswa) {
            maha.displayInfo();
        }

        System.out.println("\n" + "==================================" +
                            "\n" + "Jumlah Mahasiswa yang didata : " + mahasiswa.size());
    }
}
