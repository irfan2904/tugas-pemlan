package StudiKasus2.src.p;

class Mahasiswa {
    private String nim;
    private String nama;
    private String alamat;

    public Mahasiswa(String nim, String nama, String alamat) {
        this.nim = nim;
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNim() {
        return nim;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void displayInfo() {
        System.out.println("\n" +"NIM\t\t: " + getNim() +
                            "\n" + "Nama\t: " + getNama() +
                            "\n" + "Alamat\t: " + getAlamat());
    }

}