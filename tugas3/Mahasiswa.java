package p;

import java.util.ArrayList;

public class Mahasiswa {
    private String nama;
    private String nim;
    private ArrayList<MataKuliah> matKul = new ArrayList<>();


    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public ArrayList<MataKuliah> getMatkul() {
        return matKul;
    }

    public void setMatKul(ArrayList<MataKuliah> matKul) {
        this.matKul = matKul;
    }

    public String displayInfo() {
        return "NIM\t\t: " + getNim() +
                "\nNama\t: " + getNama();
    }
}

