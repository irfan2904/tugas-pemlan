package p;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList mahasiswaList = new ArrayList();

        Scanner in = new Scanner(System.in);
        boolean next = true;
        while (next) {
            System.out.print("Masukkan NIM : ");
            String nim = in.nextLine();

            System.out.print("Masukkan Nama : ");
            String nama = in.nextLine();

            Mahasiswa mhs = new Mahasiswa();
            mhs.setNim(nim);
            mhs.setNama(nama);

            ArrayList<MataKuliah> mataKuliahList = new ArrayList<>();
            boolean nextMataKuliah = true;
            while (nextMataKuliah) {
                MataKuliah matKul = new MataKuliah();

                System.out.print("Masukkan Kode Mata Kuliah : ");
                String kodeMatkul = in.nextLine();

                System.out.print("Masukkan Nama Mata Kuliah : ");
                String namaMatkul = in.nextLine();

                System.out.print("Masukkan Nilai Angka : ");
                int nilaiAngka;
                try {
                    nilaiAngka = (Integer.parseInt(in.nextLine()));
                } catch (NumberFormatException e) {
                    System.out.println("Nilai harus berupa angka!");
                    continue;
                }

                matKul.setKodeMatkul(kodeMatkul);
                matKul.setNamaMatkul(namaMatkul);
                matKul.setNilaiAngka(nilaiAngka);

                mataKuliahList.add(matKul);
                System.out.print("\nTambah Mata Kuliah? ('tekan 't' jika tidak) : ");
                String tambahMatKul = in.nextLine();
                System.out.println();

                if (tambahMatKul.equals("t")) {
                    nextMataKuliah = false;
                }
            }

            mhs.setMatKul(mataKuliahList);
            mahasiswaList.add(mhs);
            System.out.print("Tambah Mahasiswa Lagi? (tekan 't' jika tidak) : ");
            String tambah = in.nextLine();
            System.out.println();

            if (tambah.equals("t")) {
                next = false;
            }
        }

        for (int i = 0; i < mahasiswaList.size(); i++) {
            Mahasiswa mhs = (Mahasiswa) mahasiswaList.get(i);
            System.out.println("==================================");
            System.out.println("\t\tKartu Hasil Studi\n" +
                                "\n" + mhs.displayInfo());
            System.out.println();

            ArrayList mataKuliahList = mhs.getMatkul();
            for (int j = 0; j < mataKuliahList.size(); j++) {
                MataKuliah mata = (MataKuliah) mataKuliahList.get(j);
                System.out.println(mata.displayInfoMatKul());
            }
        }
    }
}

