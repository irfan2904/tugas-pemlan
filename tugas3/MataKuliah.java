package p;

public class MataKuliah {
    private String kodeMatkul;
    private String namaMatkul;
    private int nilaiAngka;

    public String getKodeMatkul() {
        return kodeMatkul;
    }

    public void setKodeMatkul(String kodeMatkul) {
        this.kodeMatkul = kodeMatkul;
    }

    public String getNamaMatkul() {
        return namaMatkul;
    }

    public void setNamaMatkul(String namaMatkul) {
        this.namaMatkul = namaMatkul;
    }

    public int getNilaiAngka() {
        return nilaiAngka;
    }

    public void setNilaiAngka(int nilaiAngka) {
        this.nilaiAngka = nilaiAngka;
    }

    public String getNilaiAlphabet() {
        if (nilaiAngka >= 80) {
            return "A";
        } else if (nilaiAngka >= 60) {
            return "B";
        } else if (nilaiAngka >= 50) {
            return "C";
        } else if (nilaiAngka >= 40) {
            return "D";
        } else {
            return "E";
        }
    }

    public String displayInfoMatKul () {
        return " - |\t" + getKodeMatkul() +
                "\t|\t" + getNamaMatkul() +
                "\t|\t" + getNilaiAlphabet() + "\t|";
    }
}


